import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignIconComponent } from './design-icon.component';

describe('DesignIconComponent', () => {
  let component: DesignIconComponent;
  let fixture: ComponentFixture<DesignIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
